
<?php

namespace Tarunsmtgroup\Custompackage;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CustompackageController extends Controller
{
    public function index(){
    	return view('custompackage::test',[]);
    }

    public function postmapmarker(Request $request){

        $raduis = !empty($request->Raduis) ? (int)$request->Raduis : 50 ;
    
        if(!empty($request->Place)){
            $getCordinates   = City::getCityCordinates($request->Place);
    
            if($getCordinates == null){
                return response()->json(['error'=> "Location not found"]);
            }else{
                $latitude  =  $getCordinates['lat'];
                $longitude =  $getCordinates['lng'];
            }
        }else{
            if(!empty($request->lat) && !empty($request->lng)){
                session()->put('lat',$request->lat);
                session()->put('lng',$request->lng);
                $latitude  = session()->get('lat');
                $longitude = session()->get('lng');
            }else{
                $latitude  = session()->get('lat');
                $longitude = session()->get('lng');
            }
        }
        $companies      =       DB::table('companies');
        $companies      =       $companies->select("*", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                                * cos(radians(company_lat)) * cos(radians(company_lng) - radians(" . $longitude . "))
                                                + sin(radians(" .$latitude. ")) * sin(radians(company_lat))) AS distance"));
        $companies      =       $companies->having('distance', '<', $raduis);
        $companies      =       $companies->orderBy('distance', 'asc');
    
        $companies      =       $companies->get();
        if(count($companies) > 0)
        {
        foreach ($companies as $key => $companies) {
            if($companies->company_lat <= 0 && $companies->company_lng <= 0){
                $city_id = Company::find($companies->id,['city_id']);
                $lat = $city_id->city->lat;
                $lng = $city_id->city->lng;
            }
            else{
                $lat = $companies->company_lat;
                $lng = $companies->company_lng;
            }
            $map_markes[] = (object)array(
                'id'             => $companies->id,
                'coords_lat'     => $lat,
                'coords_lng'     => $lng,
                'location_email' => $companies->email,
                'city'           => $companies->citta,
                'location_title' => $companies->rag_soc,
                'indirizzo'      => $companies->indirizzo,
                'nazione'        => $companies->nazione,
                'telefono'       => $companies->telefono,
                'email'          => $companies->email
            );
        }
        return response()->json($map_markes);
       }
       return response()->json(['error'=> "Company not found"]);
    
    
    
    }
    
    public function notices()
    {
                 return view('nearby.notices');
    
    }
    
    public function getcompanydata(Request $request){
           $data        = Company::find($request->id);
           $quotes      = KillerQuote::where('company_id','=',$request->id)->get();
           $deals       = Deal::where('company_id','=',$request->id)->get();
           $activeLangs = \Areaseb\Core\Models\Setting::ActiveLangs();
           if($data->telefono){
                if($data->nazione != "" || !is_null($data->nazione)){
                    if($data->nazione == 'IT'){
                       $data->telefono = "+39".$data->telefono;
                    }else{
                        $prefix = '';
                        $c = \Areaseb\Core\Models\Country::where('iso2', $data->nation)->first();
                        if($c){
                            $prefix = "+".$c->phone_code;
                        }
                        $data->telefono  = $prefix.$data->nazione;
                    }
                }else{
                    $data->telefono = $data->telefono;
                }
            }
    
           foreach ($data->invoices as $invoice) {
    
            $invoice->status     =   $invoice->getStatusFormattedAttribute();
            $invoice->total      =   $invoice->total_formatted;
        }
    
           foreach($data->events as $event){
           // array_push($events,$event);
           }
    
           foreach($quotes as $quote){
               $bg = '';
               if( ( $quote->expirancy_date->lt(\Carbon\Carbon::now()) ) && (is_null($quote->accepted)) )
                {
                    $bg = 'bg-warning';
                }
    
                if($quote->accepted === null)
                {
                    $sort = 1;
                    $badge = '<span class="badge badge-default">In attesa</span>';
                }
                else
                {
                    if($quote->accepted)
                    {
                        $sort = 2;
                        $badge = '<span class="badge badge-success">Accettato</span>';
                    }
                    else
                    {
                        $sort = 3;
                        $badge = '<span class="badge badge-danger">Rifiutato</span>';
                    }
                }
            //  $quote->importo = $quote->getCleanImportoAttribute();
    
            //  $quote->importo = $quote->getImportoAttribute();
             if(is_null($quote->filename)){
                 if($quote->company->lingua != 'it'){
                     if(in_array($quote->company->lingua, $activeLangs)){
                         $quote->activelangs = true;
                     }
                    $quote->activelangs = false;
                 }
             }
             $quote->total   = $quote->getCalculateImportoAttribute();
             $quote->bg      = $bg;
             $quote->sort    = $sort;
             $quote->badge   = $badge;
    
             //$quote->importo = number_format((float)$quote->importo, 2, ',', '.');
           }
           return response()->json(['data' => $data,'quotes'=> $quotes,'deals'=>$deals]);
    }
    
    public function sendPdf(Request $request, $id)
        {
            $quote = KillerQuote::find($id);
            if(is_null($quote->filename))
            {
                if($quote->company->lingua == 'it')
                {
                    $pdf = $this->generatePdfIta($quote);
                    $fileWithPath = storage_path('app/public/killerquotes/pdf/'.$quote->id.'/preventivo.pdf');
                }
                else
                {
                    $pdf = $this->generatePdf($quote, $quote->company->lingua);
                    $fileWithPath = storage_path('app/public/killerquotes/pdf/'.$quote->company->lingua.'/'.$quote->id.'/preventivo.pdf');
                }
                if (file_exists($fileWithPath))
                {
                    unlink($fileWithPath);
                }
                $pdf->save($fileWithPath);
            }
            else
            {
                $fileWithPath = storage_path('app/public/killerquotes/original/'.$quote->filename);
            }
            $mailer = app()->makeWith('custom.mailer', Setting::smtp(0));
            try
            {
                $mailer->send(new SendQuote($fileWithPath, $quote->company, $request->object, $request->body));
                return back()->with('message', 'Preventivo inviato correttamente');
            }
            catch(\Exception $e)
            {
                return back()->with('error', $e->getMessage());
            }
    
        }
    
        private function generatePdfIta($quote)
        {
            $media = [];
            foreach($quote->items as $item)
            {
                $pdf_attachment = $item->product->media()->pdf()->first();
                if($pdf_attachment)
                {
                    $media[] = storage_path('app/public/products/docs/'.$pdf_attachment->filename);
                }
            }
            if(KillerQuoteSetting::HasDefaultPdfAttachment())
            {
                $media[] = KillerQuoteSetting::DefaultPdfAttachment();
            }
    
            $base_settings = Setting::base();
            $fe_settings = Setting::fe();
            $settings = KillerQuoteSetting::assoc();
    
            $path = "public/killerquotes/pdf/{$quote->id}";
    
            if(Storage::exists($path))
                Storage::deleteDirectory($path);
    
            Storage::makeDirectory($path);
    
            $logoPdfPath = storage_path("app/{$path}/logo.pdf");
            $documentPdfPath = storage_path("app/{$path}/document.pdf");
    
            $header = View::make('killerquote::pdf.components.header', compact('settings', 'base_settings', 'fe_settings'))->render();
            $footer = View::make('killerquote::pdf.components.footer', compact('settings', 'base_settings', 'fe_settings'))->render();
    
            Storage::put("{$path}/header.html", $header);
            Storage::put("{$path}/footer.html", $footer);
    
            $headerUrl = asset("storage/killerquotes/pdf/{$quote->id}/header.html");
            $footerUrl = asset("storage/killerquotes/pdf/{$quote->id}/footer.html");
    
            $merger = PDFMerger::init();
    // return view('killerquote::pdf.logo', compact('quote', 'settings', 'base_settings', 'fe_settings'));
            $logo = PDF::loadView('killerquote::pdf.logo', compact('quote', 'settings', 'base_settings', 'fe_settings'))
                ->setPaper('a4')
                ->setOption('enable-local-file-access', true)
                ->setOption('encoding', 'UTF-8');
    // return $logo->inline();
            $logo->save($logoPdfPath);
    // return $logo->inline();
    
            $document = PDF::loadView('killerquote::pdf.quote', compact('quote', 'settings', 'base_settings', 'fe_settings'))
                ->setPaper('a4')
                ->setOption('enable-local-file-access', true)
                ->setOption('header-spacing', 10)
                //->setOption('header-html', $headerUrl)
                //->setOption('footer-html', $footerUrl)
                ->setOption('encoding', 'UTF-8');
            $document->save($documentPdfPath);
    
            $merger->addPDF($logoPdfPath, 'all', 'P');
            $merger->addPDF($documentPdfPath, 'all', 'P');
    
            foreach($media as $attachment)
            {
                $merger->addPDF($attachment, 'all', 'P');
            }
    
            // $filename = 'N'.$quote->numero.'--'.$quote->created_at->format('d-m-Y').'.pdf';
            // $merger->setFileName($filename);
            $merger->merge();
            return $merger;
        }
    
        private function generatePdf($quote, $locale)
        {
            $media = [];
            foreach($quote->items as $item)
            {
                $pdf_attachment = $item->product->media()->pdf()->first();
                if($pdf_attachment)
                {
                    $media[] = storage_path('app/public/products/docs/'.$pdf_attachment->filename);
                }
            }
    
            if(KillerQuoteSettingLocale::HasDefaultPdfAttachment($locale))
            {
                $media[] = KillerQuoteSettingLocale::DefaultPdfAttachment($locale);
            }
    
            $base_settings = Setting::base();
            $fe_settings = Setting::fe();
            $settings = KillerQuoteSettingLocale::assocLocale($locale);
            $path = "public/killerquotes/pdf/{$locale}/{$quote->id}";
    
            if(Storage::exists($path))
                Storage::deleteDirectory($path);
    
            Storage::makeDirectory($path);
    
            $logoPdfPath = storage_path("app/{$path}/logo.pdf");
            $documentPdfPath = storage_path("app/{$path}/document.pdf");
    
            $header = View::make('killerquote::pdf.components.header', compact('settings', 'base_settings', 'fe_settings'))->render();
            $footer = View::make('killerquote::pdf.components.footer', compact('settings', 'base_settings', 'fe_settings'))->render();
    
            $merger = PDFMerger::init();
    
            Storage::put("{$path}/header.html", $header);
            Storage::put("{$path}/footer.html", $footer);
    
            $headerUrl = asset("storage/killerquotes/pdf/{$locale}/{$quote->id}/header.html");
            $footerUrl = asset("storage/killerquotes/pdf/{$locale}/{$quote->id}/footer.html");
    
            $logo = PDF::loadView('killerquote::pdf.logo', compact('quote', 'settings', 'base_settings', 'fe_settings'))
                ->setPaper('a4')
                ->setOption('enable-local-file-access', true)
                ->setOption('encoding', 'UTF-8');
    // return $logo->inline();
            $logo->save($logoPdfPath);
    
    //return view('killerquote::pdf.quote', compact('quote', 'settings', 'base_settings', 'fe_settings'));
            $document = PDF::loadView('killerquote::pdf.quote', compact('quote', 'settings', 'base_settings', 'fe_settings'))
                ->setPaper('a4')
                ->setOption('enable-local-file-access', true)
                ->setOption('header-spacing', 10)
                //->setOption('header-html', $headerUrl)
                //->setOption('footer-html', $footerUrl)
                ->setOption('encoding', 'UTF-8');
    //return $document->inline();
            $document->save($documentPdfPath);
    
            $merger->addPDF($logoPdfPath, 'all', 'P');
            $merger->addPDF($documentPdfPath, 'all', 'P');
    
            foreach($media as $attachment)
            {
                $merger->addPDF($attachment, 'all', 'P');
            }
    
    
            // $filename = str_slug(trans('killerquote::kq.preventivo')).'-N'.$quote->numero.'--'.$quote->created_at->format('d-m-Y').'.pdf';
            // $merger->setFileName($filename);
            $merger->merge();
            return $merger;
        }
    
    
}
