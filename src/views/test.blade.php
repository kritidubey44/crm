{{-- <!DOCTYPE html>
<html>
<head>
	<title>Custom package</title>
</head>
<body>
	<h1 style="text-align:center">Welcom to my
		<span style="font-weight:normal">Custom package</span>
	</h1>
</body>
</html> --}}
@extends('areaseb::layouts.app')

@include('areaseb::layouts.elements.title', ['title' => 'Nelle vicinanze Aziende '])

@section('content')
<style>

/* setting the text-align property to center*/
 td {
  padding: 5px;
  text-align:center;

}
#table_detail tr:hover
{
  background-color: #F2F2F2;
}

#table_detail .hidden_row {
    display: none;
}
/* #table_detail .hiddeninner_row{
    display: none;
}   */
.project-tab {
    padding: 10%;
    margin-top: -8%;
}
.project-tab #tabs{
    background: #007b5e;
    color: #eee;
}
.project-tab #tabs h6.section-title{
    color: #eee;
}
.project-tab #tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #0062cc;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 3px solid !important;
    font-size: 16px;
    font-weight: bold;
}
.project-tab .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #0062cc;
    font-size: 16px;
    font-weight: 600;
}
.project-tab .nav-link:hover {
    border: none;
}
.project-tab thead{
    background: #f3f3f3;
    color: #333;
}
.project-tab a{
    text-decoration: none;
    color: #333;
    font-weight: 600;
}

#map {
  height: 400px;
  width: 100%;
}
</style>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript">

let map, infoWindow;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 6,
  });
  infoWindow = new google.maps.InfoWindow();
  const locationButton = document.createElement("button");
  locationButton.textContent = "Pan to Current Location";
  locationButton.classList.add("custom-map-control-button");
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
  locationButton.addEventListener("click", () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };

          infoWindow.setPosition(pos);
          infoWindow.setContent("Location found.");
          infoWindow.open(map);
          map.setCenter(pos);
          var url = `{{config('app')['url']}}get/companies?lat=`+pos.lat+`&lng=`+pos.lng;
          markerscodes(url);
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    }else{
      handleLocationError(false, infoWindow, map.getCenter());
    }
  });

  onClick = (e) => {
    e.preventDefault();
    var Place  = document.getElementById('Place').value;
    var Raduis = document.getElementById('Raduis').value;
    var url = `{{config('app')['url']}}get/companies?Place=`+Place+`&Raduis=`+Raduis;
    console.log(url);
    markerscodes(url);
  }
}

async function markerscodes(url) {
    var data = await axios(url);
    var lacationData = data.data;
    console.log(data)
    if(lacationData['error']){
      if(lacationData['error'] == "Location not found"){
            new Noty({
                text: "No Companies found on this location.",
                type: 'error',
                theme: 'bootstrap-v4',
                timeout: 2500,
                layout: 'topRight'
            }).show();

      }
      if(lacationData['error'] == "Company not found"){
        new Noty({
                text: "Company not found",
                type: 'error',
                theme: 'bootstrap-v4',
                timeout: 2500,
                layout: 'topRight'
            }).show();
      }
    }else{
        console.log(lacationData);
         mapDisplay(lacationData);
    }
  }

function mapDisplay(datas) {
  var mapElement = document.getElementById('map');
  var options = {
  center: {
      lat:Number(datas[0].coords_lat), lng:  Number(datas[0].coords_lng) },
      zoom: 10
  }
  var map = new google.maps.Map(mapElement, options );
  var markers = new Array();
  for(let index = 0; index < datas.length; index++) {
      markers.push({
                    coords: { lat: Number(datas[index].coords_lat), lng:  Number(datas[index].coords_lng) },
                    content: `<div onclick="getCompanydata(${datas[index].id})"><h5>${datas[index].location_title}</h5>
                                    <p><i class="icon address-icon"></i>${datas[index].indirizzo}</p><p>${datas[index].city}</p><small>${datas[index].telefono},${datas[index].email}</small></div>`
                  });
  }
  for(var i = 0; i < markers.length; i++ ){
        addMarker(markers[i]);
  }

  function addMarker(props){
    var marker = new google.maps.Marker({
    position: props.coords,
    map:map
  });

  if(props.iconImage){
      marker.setIcon(props.iconImage);
  }

  if(props.content){
      var infowindow = new google.maps.InfoWindow({
                          content: props.content
                       });

      marker.addListener('mouseover', function() {
            infowindow.open(map, marker);
      });
  }
 }
};

</script>

<div class="row">
        <div class="col-12">
            <div class="card pb-5">
                <div class="card-header bg-secondary-light">
                    <!--<h3 class="card-title">Nelle vicinanze Aziende </h3>-->
                    <div class="card-tools">
                    </div>
                </div>

            <div class="card-body pb-5">
               <form class="form-inline" name="form" id="form" method="GET" action="{{ route('postmapmarker') }}">
                 @csrf
                <div class="form-group mb-2">
                    <input type="text" class="form-control" name="Place" id="Place" placeholder="City,zip code,street,...">
                </div>

                <div class="form-group mx-sm-3 mb-2">
                    <input type="Number" class="form-control" name="Raduis" id="Raduis" placeholder="Raduis(KM)">
                </div>
                <button type="submit" onclick="onClick(event)" class="btn btn-primary mb-2"> GO </button>
                </form>
                <hr>
                <div id="map"></div>
                <div id="results_found"></div>
            </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="quote-modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Invia Preventivo al cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                {!! Form::open(['url' => '#']) !!}
                    <div class="form-group">
                        <label>Oggetto</label>
                        {{Form::text('object', null, ['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Corpo Email</label>
                        {{Form::textarea('body', null, ['class' => 'form-control'])}}
                    </div>

                    <button type="submit" class="btn btn-success btn-block">Invia</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>

<script src="http://maps.googleapis.com/maps/api/js?key={{config('googlemap')['map_apikey']}}&callback=initMap&libraries=&v=weekly"
      async></script>
<script type="text/javascript">

    function showHideRow(row){
        $("#"+row).toggle();
        $("#fav").toggleClass("fa fa-chevron-right").addClass("fa fa-chevron-down");
    }
    function showInvoices(row1){
      $("#" + row1).toggle();
      console.log(row1);
    }
    function formatDate(getdate) {
        var date = new Date(getdate);
        return date.getDate() + "/" + (date.getMonth()+1)+ "/" + date.getFullYear() ;
    }


    getCompanydata = (id) => {

      var url1 = `{{config('app')['url']}}getcompanydata?id=${id}`;

      async function getData(){
        var data1 = await axios(url1);

        var companydata = data1.data;

        console.log(companydata);
        const invoices = companydata['data'].invoices.map((item) => {
          return(
            `<tr>
              <td>${item.numero}</td>
              <td>${formatDate(item.data)}</td>
              <td>${item.imponibile}</td>
              <td>${formatDate(item.data_scadenza)}</td>
              <td>${item.total}</td>
              <td>${item.status}</td>
              <td>
                <span>
                <a href ="{{config('app')['url']}}invoices/${item.id}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                <a href="#" onclick="sendToClient(event)" target="_blank" data-id="${item.id}" class="btn btn-success btn-sm ml-1 sendToClient"><i class="fa fa-envelope"></i>
                   Send PDF
                <a>
                 <a href="#" target="_blank" data-id="" class="btn btn-primary btn-sm ml-1 sendToClient"><i class="fa fa-eye"></i>
                   View Notice
                <a>
                </span>
              </td>
            <tr>`
          );
        });
        const events = companydata['data'].events.map((item) => {
          return(
            `<tr>
              <td>${item.starts_at}</td>
              <td>${item.title}</td>
              <td>${item.summary}</td>
              <td>${item.location != null ? item.location : '-' }</td>
              <td>${item.done == 0 ? '<span class="badge badge-success">fatta</span>' : '<span class="badge badge-danger">Non ancora fatto</span>'}</td>
            <tr>`
          );
        });
        const quotes = companydata['quotes'].map((item) =>{
            var pdf;
            if(item.filename == null){
                console.log('yes');
                if(item.company.lingua !== 'it'){
                    if(item.activelangs == true){
                        pdf = '<a target="_BLANK" href="{{config("app")["url"]}}killerquotes/'+item.id+'/pdf/'+item.company.lingua+'" title="Esporta PDF" class="btn btn-primary btn-icon btn-sm"><i class="fa fa-file-pdf"></i></a>';
                    }
                }else{
                    pdf = '<a target="_BLANK" href="{{config("app")["url"]}}killerquotes/'+item.id+'/pdf" title="Esporta PDF" class="btn btn-primary btn-icon btn-sm"><i class="fa fa-file-pdf"></i></a>';
                }
            }else{
                pdf = '<a target="_BLANK" href="{{config("app")["url"]}}storage/killerquotes/original/'+item.filename+'" title="Apri PDF" class="btn btn-primary btn-icon btn-sm"><i class="fa fa-file-pdf"></i></a>';
            }

          return(
            `
            <tr id="row-${item.id}">
                <td class="${item.bg}">${item.numero}</td>
                <td class="${item.bg}">${formatDate(item.created_at)}</td>
                <td class="${item.bg}" data-order="${item.importo}">${item.importo}</td>
                <td data-order="${item.expirancy_date}">${formatDate(item.expirancy_date)}</td>
                <td class="${item.bg}">${item.total}</td>
                <td class="${item.bg}" data-order="${item.sort}">${item.badge}</td>
                <td>
                <a href="#" onclick="sendQuote(event);" title="Invia al cliente" data-numero="${item.numero}" data-date="${formatDate(item.created_at)}" data-id="${item.id}" data-company="${item.company.rag_soc}" class="btn btn-info btn-icon btn-sm sendQuote"><i class="far fa-paper-plane"></i></a>
                ${pdf}
                </td>
            <tr>
            `
          );
        });
        const deals = companydata['deals'].map((item) => {
          return(
            `
            <tr>
              <td>${item.numero}</td>
              <td>${formatDate(item.created_at)}</td>
              <td>${item.notes}</td>
              <td>${item.accepted == null ? '<span class="badge badge-primary">Aperta</span>' : item.accepted == 0 ? '<span class="badge badge-danger">Chiusa</span>' : '<span class="badge badge-success">Completata</span>'}</td>
              <td><a href="{{config('app')['url']}}deals/1/edit" class="btn btn-primary btn-icon btn-sm"><i class="fa fa-eye"></i></a></td>
            </tr>
            `
          );
        });
        const html =
              `<tr>
                <td onclick="showHideRow('hidden_row');" ><i id="fav" class="fa fa-chevron-right" aria-hidden="true"></i></td>
                <td>${companydata['data'].rag_soc}</td>
                <td>${companydata['data'].indirizzo}</td>
                <td>${companydata['data'].telefono != null ? companydata['data'].telefono : '-' }</td>
                <td>${companydata['data'].email}</td>
                <td>
                <a target="_BLANK" href="{{config('app')['url']}}deals/create/?company_id=${companydata['data'].id}" class="btn btn-secondary btn-icon btn-sm" title="crea trattative"><i class="fas fa-handshake"></i></a>
                <a target="_BLANK" href="{{config('app')['url']}}killerquotes/create?company_id=${companydata['data'].id}" class="btn btn-secondary btn-icon btn-sm" title="crea trattative"><i class="fas fa-file-invoice-dollar"></i></a>
              </td>
              </tr>
                <tr id="hidden_row" class="hidden_row">
                  <td colspan="6">
                    <div class="row">
                    <div class="col-md-12">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                          <a style="background-color:#98FB98;" class="nav-item nav-link active" id="nav-invoices-tab" data-toggle="tab" href="#nav-invoices" role="tab" aria-controls="nav-invoices" aria-selected="true"><strong>Invoices</strong></a>&nbsp;
                          <a style="background-color:#AFEEEE;" class="nav-item nav-link" id="nav-events-tab" data-toggle="tab" href="#nav-events" role="tab" aria-controls="nav-events" aria-selected="false"><strong>Events</strong></a>&nbsp;
                          <a style="background-color:#F08080;" class="nav-item nav-link" id="nav-quotes-tab" data-toggle="tab" href="#nav-quotes" role="tab" aria-controls="nav-quotes" aria-selected="false"><strong>Quotes</strong></a>&nbsp;
                          <a style="background-color:#F0E68C;" class="nav-item nav-link" id="nav-deals-tab" data-toggle="tab" href="#nav-deals" role="tab" aria-controls="nav-deals" aria-selected="false"><strong>Deals</strong></a>
                        </div>
                    </nav>
                      <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-invoices" role="tabpanel" aria-labelledby="nav-invoices-tab">
                          <table class="table" cellspacing="0">
                                <thead>
                                  <th>NR</th>
                                  <th>Data</th>
                                  <th>Quantità</th>
                                  <th>data di scadenza</th>
                                  <th>Totale</th>
                                  <th>Stato</th>
                                  <th>Azione</th>
                                </thead>
                                <tbody>
                                <tr>
                                 <div class="form-group mr-2 mb-4 mt-4" style="float:right;">
                                    <a class="btn btn-primary" href="{{config('app')['url']}}/invoices/create?company_id=${companydata['data'].id}"><i class="fas fa-plus">Crea Fattura</i></a></tr>
                                 </div>
                                </tr>
                                ${invoices.length > 0 ? invoices : '<tr><td colspan="7">Risultato non trovato</td></tr>'}
                                </tbody>
                            </table>
                                </div>
                                <div class="tab-pane fade" id="nav-events" role="tabpanel" aria-labelledby="nav-events-tab">
                                    <table class="table" cellspacing="0">
                                      <thead>
                                        <th>Data e ora</th>
                                        <th>Titolo</th>
                                        <th>Descrizione</th>
                                        <th>Posto</th>
                                        <th>Fatto</th>
                                      </thead>
                                      <tbody>
                                      ${events.length > 0 ? events : '<tr><td colspan="5">Risultato non trovato</td></tr>'}
                                      </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="nav-quotes" role="tabpanel" aria-labelledby="nav-quotes-tab">
                                    <table class="table" cellspacing="0">
                                      <thead>
                                      <th>Nr</th>
                                      <th>Data</th>
                                      <th>Quantità</th>
                                      <th>data di scadenza</th>
                                      <th>Totale</th>
                                      <th>Stato</th>
                                      <th>Azione</th>

                                      </thead>
                                        <tbody>
                                            <div class="form-group mr-2 mb-4 mt-4" style="float:right;">
                                            <a class="btn btn-primary" href="{{config('app')['url']}}/killerquotes/create?company_id=${companydata['data'].id}"><i class="fas fa-plus">Killer</i></a></tr>
                                            </div>
                                            ${quotes.length > 0 ? quotes : '<tr><td colspan="7">Risultato non trovato</td></tr>'}
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="nav-deals" role="tabpanel" aria-labelledby="nav-deals-tab">
                                    <table class="table" cellspacing="0">
                                        <thead>
                                        <th>Nr</th>
                                        <th>Data</th>
                                        <th>Citazioni</th>
                                        <th>Stato</th>
                                        <th>Azione</th>
                                        </thead>
                                        <tbody>
                                        ${deals.length > 0 ? deals : '<tr><td colspan="5">Risultato non trovato</td></tr>'}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                          </div>
                      </div>
                    </td>
                </tr>
                `;

          $('#results_found').html(
            `<hr>
              <h5>Risultati Trovati</h5>
                <table class="table table-bordered" id="table_detail">
                  <thead>
                </thead>
                  <tbody>
                    ${html}
                  </tbody>
                </table>`);

      }
      getData();
}
function sendToClient(e){
    e.preventDefault();
    let token = "{{csrf_token()}}";
    $.post(baseURL+'pdf/send/'+$('a.sendToClient').attr('data-id'), {_token: token}).done(function( response ) {
        console.log(response);
        if(response == 'done')
        {
            new Noty({
                text: "Email Inviata",
                type: 'success',
                theme: 'bootstrap-v4',
                timeout: 2500,
                layout: 'topRight'
            }).show();
        }
        else if(response == 'error')
        {
            new Noty({
                text: "Errore",
                type: 'error',
                theme: 'bootstrap-v4',
                timeout: 2500,
                layout: 'topRight'
            }).show();
        }
        else
        {
            new Noty({
                text: response,
                type: 'warning',
                theme: 'bootstrap-v4',
                timeout: 2500,
                layout: 'topRight'
            }).show();
        }
    });
}
</script>
@section('scripts')
    <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
const smOptions = {
            height: 180,
            toolbar: [
                ['font', ['bold', 'italic']],
            ]
        };

function sendQuote(e){
    e.preventDefault();
    let oggetto = "Invio preventivo N. "+$('a.sendQuote').attr('data-numero')+" del "+$('a.sendQuote').attr('data-date');
    let testo = 'Spett. '+ $('a.sendQuote').attr('data-company');
    let action = baseURL+'killerquotes/'+$('a.sendQuote').attr('data-id')+'/send';
    $('#quote-modal').modal('show');
    $('#quote-modal input[name="object"]').val(oggetto);
    $('#quote-modal textarea').val(testo);
    $('#quote-modal textarea').summernote(smOptions);
    $('#quote-modal form').attr('action', action);
};
</script>
@stop
@endsection
